package commande;

import client.Client;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Commande")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "commande_id")
    private int idCom;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "client_id_client")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getIdCom() {
        return idCom;
    }

    public void setIdCom(int idCom) {
        this.idCom = idCom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "idCom=" + idCom +
                ", date=" + date +
                ", client=" + client +
                '}';
    }
}
