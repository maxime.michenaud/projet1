package commande;

import java.util.List;

public interface CommandeService {
    void create(Commande commande);
    Commande getCommandeById(int id);
    List<Commande> list();
    void delete (Commande commande);
    void update (Commande commande);
}
