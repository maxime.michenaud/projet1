package commande;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class CommandeRepository {

    private final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory( "bd_ventes_jpa" );
    private final EntityManager entityManager = emFactory.createEntityManager();


    public void insertCommande(Commande commande){
        entityManager.getTransaction().begin();
        entityManager.persist(commande);
        entityManager.getTransaction().commit();

    }

    public void updateCommande(Commande commande){
        entityManager.getTransaction().begin();
        entityManager.merge(commande);
        entityManager.getTransaction().commit();

    }

    public void removeCommande(Commande commande){
        entityManager.getTransaction().begin();
        entityManager.remove(commande);
        entityManager.getTransaction().commit();

    }

    public List<Commande> commandeList (){
        return entityManager.createQuery("SELECT c FROM Commande c").getResultList();
    }

    public Commande getCommandeById(int id){
        return entityManager.find(Commande.class, id);
    }

}
