package commande;

import java.util.List;

public class CommandeServiceImpl implements CommandeService{

    private final CommandeRepository commandeRepository = new CommandeRepository();

    @Override
    public void create(Commande commande) {
        commandeRepository.insertCommande(commande);
    }

    @Override
    public Commande getCommandeById(int id) {
        return commandeRepository.getCommandeById(id);
    }

    @Override
    public List<Commande> list() {
        return commandeRepository.commandeList();
    }

    @Override
    public void delete(Commande commande) {
        commandeRepository.removeCommande(commande);

    }

    @Override
    public void update(Commande commande) {

    }
}
