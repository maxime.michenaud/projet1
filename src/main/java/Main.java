import client.Client;
import client.ClientServiceImpl;
import commande.CommandeServiceImpl;
import detailclient.DetailClient;
import commande.Commande;
import detailclient.DetailClientServiceImpl;
import fournisseur.Fournisseur;
import fournisseur.FournisseurServiceImpl;
import produit.Produit;
import produit.ProduitServiceImpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main( String[ ] args ) {

        ClientServiceImpl clientService = new ClientServiceImpl();
        DetailClientServiceImpl detailClientService = new DetailClientServiceImpl();
        CommandeServiceImpl commandeService = new CommandeServiceImpl();
        FournisseurServiceImpl fournisseurService = new FournisseurServiceImpl();
        ProduitServiceImpl produitService = new ProduitServiceImpl();

        DetailClient detailClient = new DetailClient();
        detailClient.setAdresse("mon adresse");
        detailClient.setMail("monmail");
        detailClient.setTel("000000");
        Client client = new Client();
        client.setNom("Mic");
        client.setPrenom("Maxime");

        Client client2 = new Client();
        client2.setNom("bla");
        client2.setPrenom("blabla");
        clientService.create(client);
        clientService.create(client2);
        detailClientService.create(detailClient);

        client.setDetailClient(detailClient);
        detailClient.setClient(client);
        detailClientService.update(detailClient);
        clientService.update(client);

        Commande commande1 = new Commande();
        commande1.setDate(new Date());
        commande1.setClient(client);

        Commande commande2 = new Commande();
        commande2.setDate(new Date());
        commande2.setClient(client);

        commandeService.create(commande1);
        commandeService.create(commande2);

        List<Commande> list = new ArrayList<>();
        list.add(commande1);
        list.add(commande2);
        client.setCommandes(list);

        clientService.update(client);
        clientService.delete(client2);

        Produit produit1 = new Produit();
        Produit produit2 = new Produit();
        Fournisseur fournisseur = new Fournisseur();

        fournisseur.setNomFour("mon fournisseur");
        fournisseur.setAdresseFour("adresse fournisseur");

        produit1.setPrix(22);
        produit1.setLibelle("produit1");

        produit2.setPrix(89);
        produit2.setLibelle("produit2");

        produitService.create(produit1);
        produitService.create(produit2);
        fournisseurService.create(fournisseur);

        List<Produit> produitList = new ArrayList<>();
        produitList.add(produit1);
        produitList.add(produit2);
        fournisseur.setProduitList(produitList);
        produit1.setFournisseur(fournisseur);
        produit2.setFournisseur(fournisseur);

        produitService.update(produit1);
        produitService.update(produit2);

        fournisseurService.update(fournisseur);

    }

}
