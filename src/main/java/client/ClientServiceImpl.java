package client;

import java.util.List;

public class ClientServiceImpl implements ClientService{

    private final ClientRepository clientRepository = new ClientRepository();

    @Override
    public void create(Client client) {
        clientRepository.insertClient(client);
    }

    @Override
    public Client getClientById(int id) {
        return clientRepository.getClientById(id);
    }

    @Override
    public List<Client> list() {
        return clientRepository.clientList();
    }

    @Override
    public void delete(Client client) {
        clientRepository.removeClient(client);

    }

    @Override
    public void update(Client client) {
        clientRepository.updateClient(client);
    }
}
