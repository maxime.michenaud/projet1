package client;

import java.util.List;

public interface ClientService {
    void create(Client client);
    Client getClientById(int id);
    List<Client> list();
    void delete (Client client);
    void update (Client client);
}
