package client;
import commande.Commande;
import detailclient.DetailClient;
import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "Client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="client_id")
    private int idClient;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @OneToOne
    @JoinColumn(name = "detail_client_id_detail")
    private DetailClient detailClient;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Commande> commandes;

    public DetailClient getDetailClient() {
        return detailClient;
    }

    public void setDetailClient(DetailClient detailClient) {
        this.detailClient = detailClient;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idClient=" + idClient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", detailClient=" + detailClient +
                ", commandes=" + commandes +
                '}';
    }
}
