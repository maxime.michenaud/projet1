package client;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.security.PublicKey;
import java.util.List;

public class ClientRepository {

    private final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory( "bd_ventes_jpa" );
    private final EntityManager entityManager = emFactory.createEntityManager();


    public void insertClient(Client client){
        entityManager.getTransaction().begin();
        entityManager.persist(client);
        entityManager.getTransaction().commit();

    }

    public void updateClient(Client client){
        entityManager.getTransaction().begin();
        entityManager.merge(client);
        entityManager.getTransaction().commit();

    }

    public void removeClient(Client client){
        entityManager.getTransaction().begin();
        entityManager.remove(client);
        entityManager.getTransaction().commit();

    }

    public List<Client> clientList (){
        return entityManager.createQuery("SELECT c FROM Client c").getResultList();
    }

    public Client getClientById(int id){
        return entityManager.find(Client.class, id);
    }

}
