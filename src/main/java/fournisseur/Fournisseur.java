package fournisseur;

import jakarta.persistence.*;
import produit.Produit;

import java.util.List;

@Entity
@Table(name = "Fournisseur")
public class Fournisseur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fournisseur_id")
    private long idFour;
    @Column(name = "nom")
    private String nomFour;
    @Column(name = "adresse")
    private String adresseFour;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Produit> produitList;

    public long getIdFour() {
        return idFour;
    }

    public void setIdFour(long idFour) {
        this.idFour = idFour;
    }

    public String getNomFour() {
        return nomFour;
    }

    public void setNomFour(String nomFour) {
        this.nomFour = nomFour;
    }

    public String getAdresseFour() {
        return adresseFour;
    }

    public void setAdresseFour(String adresseFour) {
        this.adresseFour = adresseFour;
    }

    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }

    @Override
    public String toString() {
        return "Fournisseur{" +
                "idFour=" + idFour +
                ", nomFour='" + nomFour + '\'' +
                ", adresseFour='" + adresseFour + '\'' +
                ", produitList=" + produitList +
                '}';
    }
}
