package fournisseur;

import java.util.List;

public class FournisseurServiceImpl implements FournisseurService{

    private final FournisseurRepository fournisseurRepository = new FournisseurRepository();

    @Override
    public void create(Fournisseur fournisseur) {
        fournisseurRepository.insertFournisseur(fournisseur);
    }

    @Override
    public Fournisseur getFournisseurById(long id) {
        return fournisseurRepository.getFournisseurById(id);
    }

    @Override
    public List<Fournisseur> list() {
        return fournisseurRepository.fournisseurList();
    }

    @Override
    public void delete(Fournisseur fournisseur) {
        fournisseurRepository.removeFournisseur(fournisseur);
    }

    @Override
    public void update(Fournisseur fournisseur) {
        fournisseurRepository.updateFournisseur(fournisseur);
    }
}
