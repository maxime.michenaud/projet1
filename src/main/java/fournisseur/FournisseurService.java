package fournisseur;

import java.util.List;

public interface FournisseurService {
    void create(Fournisseur fournisseur);
    Fournisseur getFournisseurById(long id);
    List<Fournisseur> list();
    void delete (Fournisseur fournisseur);
    void update (Fournisseur fournisseur);
}
