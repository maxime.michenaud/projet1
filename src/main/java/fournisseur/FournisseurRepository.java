package fournisseur;

import commande.Commande;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class FournisseurRepository {

    private final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory( "bd_ventes_jpa" );
    private final EntityManager entityManager = emFactory.createEntityManager();


    public void insertFournisseur(Fournisseur fournisseur){
        entityManager.getTransaction().begin();
        entityManager.persist(fournisseur);
        entityManager.getTransaction().commit();

    }

    public void updateFournisseur(Fournisseur fournisseur){
        entityManager.getTransaction().begin();
        entityManager.merge(fournisseur);
        entityManager.getTransaction().commit();

    }

    public void removeFournisseur(Fournisseur fournisseur){
        entityManager.getTransaction().begin();
        entityManager.remove(fournisseur);
        entityManager.getTransaction().commit();

    }

    public List<Fournisseur> fournisseurList (){
        return entityManager.createQuery("SELECT f FROM Fournisseur f").getResultList();
    }

    public Fournisseur getFournisseurById(long id){
        return entityManager.find(Fournisseur.class, id);
    }

}
