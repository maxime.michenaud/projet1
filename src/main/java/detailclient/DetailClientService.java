package detailclient;

import java.util.List;

public interface DetailClientService {
    void create(DetailClient detailClient);
    DetailClient getDetailClientById(int id);
    List<DetailClient> list();
    void delete (DetailClient detailClient);
    void update (DetailClient detailClient);
}
