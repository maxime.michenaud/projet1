package detailclient;

import java.util.List;

public class DetailClientServiceImpl implements DetailClientService{

    private final DetailClientRepository detailClientRepository = new DetailClientRepository();

    @Override
    public void create(DetailClient detailClient) {
        detailClientRepository.insertDetailClient(detailClient);
    }

    @Override
    public DetailClient getDetailClientById(int id) {
        return detailClientRepository.getDetailClientById(id);
    }

    @Override
    public List<DetailClient> list() {
        return detailClientRepository.detailClientList();
    }

    @Override
    public void delete(DetailClient detailClient) {
        detailClientRepository.removeDetailClient(detailClient);

    }

    @Override
    public void update(DetailClient detailClient) {
        detailClientRepository.updateDetailClient(detailClient);
    }
}
