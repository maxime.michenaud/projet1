package detailclient;

import client.Client;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class DetailClientRepository {

    private final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory( "bd_ventes_jpa" );
    private final EntityManager entityManager = emFactory.createEntityManager();


    public void insertDetailClient(DetailClient detailClient){
        entityManager.getTransaction().begin();
        entityManager.persist(detailClient);
        entityManager.getTransaction().commit();

    }

    public void updateDetailClient(DetailClient detailClient){
        entityManager.getTransaction().begin();
        entityManager.merge(detailClient);
        entityManager.getTransaction().commit();

    }

    public void removeDetailClient(DetailClient detailClient){
        entityManager.getTransaction().begin();
        entityManager.remove(detailClient);
        entityManager.getTransaction().commit();

    }

    public List<DetailClient> detailClientList (){
        return entityManager.createQuery("SELECT d FROM DetailClient d").getResultList();
    }

    public DetailClient getDetailClientById(int id){
        return entityManager.find(DetailClient.class, id);
    }

}
