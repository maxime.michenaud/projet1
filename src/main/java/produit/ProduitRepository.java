package produit;

import commande.Commande;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class ProduitRepository {

    private final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory( "bd_ventes_jpa" );
    private final EntityManager entityManager = emFactory.createEntityManager();


    public void insertProduit(Produit produit){
        entityManager.getTransaction().begin();
        entityManager.persist(produit);
        entityManager.getTransaction().commit();

    }

    public void updateProduit(Produit produit){
        entityManager.getTransaction().begin();
        entityManager.merge(produit);
        entityManager.getTransaction().commit();

    }

    public void removeProduit(Produit produit){
        entityManager.getTransaction().begin();
        entityManager.remove(produit);
        entityManager.getTransaction().commit();

    }

    public List<Produit> produitList (){
        return entityManager.createQuery("SELECT p FROM Produit p").getResultList();
    }

    public Produit getProduitById(long id){
        return entityManager.find(Produit.class, id);
    }
}
