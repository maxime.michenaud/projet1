package produit;

import fournisseur.Fournisseur;
import jakarta.persistence.*;

@Entity
@Table(name = "Produit")
public class Produit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "produit_id")
    private long idProd;
    @Column(name = "libelle")
    private String libelle;
    @Column(name = "prix")
    private float prix;

    @ManyToOne
    @JoinColumn(name = "fournisseur_fournisseur_id")
    private Fournisseur fournisseur;

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public long getIdProd() {
        return idProd;
    }

    public void setIdProd(long idProd) {
        this.idProd = idProd;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "idProd=" + idProd +
                ", libelle='" + libelle + '\'' +
                ", prix=" + prix +
                '}';
    }
}
