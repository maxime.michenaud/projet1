package produit;

import java.util.List;

public class ProduitServiceImpl implements ProduitService{

    private final ProduitRepository produitRepository = new ProduitRepository();

    @Override
    public void create(Produit produit) {
        produitRepository.insertProduit(produit);
    }

    @Override
    public Produit getProduitById(long id) {
        return produitRepository.getProduitById(id);
    }

    @Override
    public List<Produit> list() {
        return produitRepository.produitList();
    }

    @Override
    public void delete(Produit produit) {
        produitRepository.removeProduit(produit);
    }

    @Override
    public void update(Produit produit) {
        produitRepository.updateProduit(produit);

    }
}
