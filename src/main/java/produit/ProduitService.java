package produit;

import java.util.List;

public interface ProduitService {
    void create(Produit produit);
    Produit getProduitById(long id);
    List<Produit> list();
    void delete (Produit produit);
    void update (Produit produit);
}
